cd "${0%/*}"
echo "Building creanciers service"
# -t for enabling tests.
if [[ "$1" == -t || $2 == -t ]]
then
mvn install package
else
mvn install package -DskipTests
fi
mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)
docker build -t creanciers_service:latest .
if [[ "$1" == -d || "$2" == -d ]]
then
echo "Deploying creanciers service"
docker-compose up
fi
