package com.ensapay.creanciers;

import com.ensapay.creanciers.models.Creance;
import com.ensapay.creanciers.models.Creancier;
import com.ensapay.creanciers.repositories.CreanceRepository;
import com.ensapay.creanciers.repositories.CreancierRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SpringBootApplication
@EnableSwagger2
@EnableFeignClients
@RequiredArgsConstructor
@Log4j2
public class CreanciersApplication implements CommandLineRunner {

    private final CreancierRepository creancierRepository;
    private final CreanceRepository creanceRepository;
    public static void main(String[] args) {

        SpringApplication.run(CreanciersApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        if(creancierRepository.findAll().size()==0){
            Creance creance0 = Creance.builder().name("internet").category("Operator").build();
            Creance creance1 = Creance.builder().name("appels").category("Operator").build();
            Creance creance2 = Creance.builder().name("water").category("Water&&Electricity").build();
            Creance creance3 = Creance.builder().name("electrecity").category("Water&&Electricity").build();
            creanceRepository.deleteAll();
            creanceRepository.save(creance0);creanceRepository.save(creance1);creanceRepository.save(creance2);
            creanceRepository.save(creance3);


            Creancier iam = Creancier.builder().codeCreancier("iam")
                    .creances(creanceRepository.findAllByCategory("Operator"))
                    .name("MAROC TELECOM").build();

            Creancier rademaa = Creancier.builder().codeCreancier("rademaa")
                    .creances(creanceRepository.findAllByCategory("Water&&Electricity"))
                    .name("RADEMAA").build();

            Creancier orange = Creancier.builder().codeCreancier("orange")
                    .creances(creanceRepository.findAllByCategory("Operator"))
                    .name("ORANGE").build();

            creancierRepository.deleteAll();
            creancierRepository.save(iam);
            creancierRepository.save(rademaa);
            creancierRepository.save(orange);
        }

    }
}
