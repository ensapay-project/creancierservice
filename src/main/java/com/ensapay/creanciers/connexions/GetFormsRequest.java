package com.ensapay.creanciers.connexions;


import com.ensapay.creanciers.config.FeignConfig;
import com.ensapay.creanciers.models.Creance;
import com.ensapay.creanciers.models.Facture;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "dynamic-forms-service", url = "http://localhost:8000/forms",configuration = FeignConfig.class)
public interface GetFormsRequest {

    @GetMapping("/{id}")
    Creance getFormFields(@PathVariable int id);
}
