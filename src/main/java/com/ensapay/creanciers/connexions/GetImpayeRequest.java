package com.ensapay.creanciers.connexions;


import com.ensapay.creanciers.config.FeignConfig;
import com.ensapay.creanciers.models.Facture;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


@FeignClient(name = "impayes-service", url = "http://localhost:8000/factures", configuration = FeignConfig.class)
public interface GetImpayeRequest {

    @GetMapping("/{codeCreancier}")
    List<Facture> getAllFactures(@PathVariable String codeCreancier);

    @GetMapping("/{codeCreancier}/{factureId}")
    Facture getFacture(@PathVariable String codeCreancier , @PathVariable String factureId);

    @PostMapping("{creanceId}/{factureId}/payer")
    Facture setPayed(@PathVariable String creanceId, @PathVariable String factureId, @RequestBody Facture facture);

}
