package com.ensapay.creanciers.connexions;


import com.ensapay.creanciers.config.FeignConfig;
import com.ensapay.creanciers.models.Facture;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@FeignClient(name = "payment-service", url = "http://localhost:8000/payment/api",configuration = FeignConfig.class)
public interface GetPaymentRequest {

    @PostMapping("/{numeroCompte}/check")
    boolean checkFacture(@PathVariable String numeroCompte, @RequestParam double montant);

}
