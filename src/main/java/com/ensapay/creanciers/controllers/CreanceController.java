package com.ensapay.creanciers.controllers;

import com.ensapay.creanciers.models.Creance;
import com.ensapay.creanciers.models.Creancier;
import com.ensapay.creanciers.repositories.CreancierRepository;
import com.ensapay.creanciers.services.CreanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/creanciers/")
@RequiredArgsConstructor
@Api(description = "API pour les creances")
public class CreanceController {

    private final CreanceService service;
    private final CreancierRepository creancierRepository;
    /**
     * This api is just for getting creance by id .
     *
     * @param codeCreancier ,idCreance
     * @return creance
     */
    @GetMapping("/{codeCreancier}/{idCreance}")
    @ApiOperation(value = "Récupérer une creance à partir de son id")
    public Creance getCreance(@PathVariable String codeCreancier , @PathVariable String idCreance) {
        return service.get(idCreance);
    }

    /**
     * This api is just for creating new creance  .
     *
     * @param codeCreancier , creance
     * @return creance
     */
    @PostMapping("/{codeCreancier}")
    @ApiOperation(value = "cree une creance")
    public ResponseEntity<Creance> addCreance(@PathVariable String codeCreancier , @RequestBody Creance creance) {
        Creancier creancier = creancierRepository.findById(codeCreancier).orElseThrow(
                ()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Creancier introuvable!")
        );
        Creance c =  Creance.builder()
               .name(creance.getName())
               .category(creance.getCategory())
               .build();
        service.save(c);
        creancier.getCreances().add(c);
        creancierRepository.save(creancier);
        return ResponseEntity.ok(c);
    }

    /**
     * This api is just for updating a creance by id .
     *
     * @param codeCreancier , idCreance
     * @param creance
     * @return creance
     */
    @PostMapping("/{codeCreancier}/{id}")
    @ApiOperation(value = "modifier une creance")
    public ResponseEntity<Creance> editCreance(@PathVariable String codeCreancier ,
                                               @PathVariable String idCreance,@RequestBody Creance creance) {
        Creancier creancier = creancierRepository.findById(codeCreancier).orElseThrow(
                ()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Creancier introuvable!")
        );
        Creance c =  service.get(idCreance);
        c.setName(creance.getName());
        c.setCategory(creance.getCategory());
        service.save(c);
        creancier.getCreances().add(c);
        creancierRepository.save(creancier);
        return ResponseEntity.ok(c);
    }


    /**
     * This api is just for deleting creance by id .
     *
     * @param id
     */
    @DeleteMapping("/{codeCreancier}/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Supprimer une creance à partir de son id")
    public void removeCreance(@PathVariable String codeCreancier , @PathVariable String id) {
        service.delete(codeCreancier,id);
    }
}
