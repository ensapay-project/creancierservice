package com.ensapay.creanciers.controllers;


import com.ensapay.creanciers.models.Creance;
import com.ensapay.creanciers.models.Creancier;
import com.ensapay.creanciers.repositories.CreancierRepository;
import com.ensapay.creanciers.services.CreanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/creanciers")
@RequiredArgsConstructor
@Api(description = "API pour les Creanciers")
@Log4j2
public class CreancierController {

    private final CreancierRepository creancierRepository;
    private CreanceService service;

    /**
     * This api is just for getting creancier by codeCreancier .
     *
     * @param codeCreancier
     * @return creancier
     */
    @GetMapping("/{codeCreancier}")
    @ApiOperation(value = "Récupérer un creancier à partir de son code")
    public Creancier getCreancier(@PathVariable String codeCreancier) {
        return creancierRepository.findById(codeCreancier).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Creancier introuvable"));
    }

    /**
     *
     *This api to retrieve all creanciers
     * @return all creancier
     */
    @GetMapping("/all")
    @ApiOperation(value = "Récupérer tous les creanciers")
    public List<Creancier> getAllCreancier()
    {
        log.info("get all creanciers");
        return creancierRepository.findAll();
    }

    /**
     * This api is just for creating new creance  .
     *
     * @param codeCreancier
     * @param id
     * @return creancier
     */
    @PostMapping("/{codeCreancier}/add-creance")
    public ResponseEntity<Creancier> addCreance(@PathVariable String codeCreancier, @RequestBody String id) {

        Creancier c = this.getCreancier(codeCreancier);
        c.setCreances((List<Creance>) service.get(id));
        creancierRepository.save(c);
        return ResponseEntity.ok(c);
    }


}
