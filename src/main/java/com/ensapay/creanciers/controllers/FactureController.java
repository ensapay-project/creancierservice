package com.ensapay.creanciers.controllers;


import com.ensapay.creanciers.connexions.GetImpayeRequest;
import com.ensapay.creanciers.connexions.GetPaymentRequest;
import com.ensapay.creanciers.models.Creance;
import com.ensapay.creanciers.models.Creancier;
import com.ensapay.creanciers.models.Facture;
import com.ensapay.creanciers.repositories.CreancierRepository;
import com.ensapay.creanciers.services.CreanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/creanciers/facture")
@RequiredArgsConstructor
@Api(description = "API pour les factures")
@Log4j2
@CrossOrigin(origins = "*")
public class FactureController {

    private final CreancierRepository creancierRepository;
    private final GetImpayeRequest getImpayeRequest;
    private final GetPaymentRequest getPaymentRequest;
    private CreanceService service;

    @GetMapping("/{codeCreancier}")
    List<Facture> getFactures(@PathVariable String codeCreancier )
    {
        return getImpayeRequest.getAllFactures(codeCreancier);
    }

    @GetMapping("/{creanceId}/{factureId}")
    Facture getFacture(@PathVariable String creanceId,@PathVariable String factureId)
    {
        log.info("fetch factures");
        return getImpayeRequest.getFacture(creanceId,factureId);
    }

    @PostMapping("/{creanceId}/{factureId}/{numeroCompte}")
    ResponseEntity<Facture> getPayed(@PathVariable String creanceId,
                                     @PathVariable String factureId,
                                    @PathVariable String numeroCompte){
        Facture facture =getFacture(creanceId,factureId);
        boolean status = getPaymentRequest.checkFacture(numeroCompte,facture.getMontantFinal());
        if(!status){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Votre solde est insuffisant !");
        }
        try{
            getImpayeRequest.setPayed(creanceId,factureId,facture);

        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Facture déjà payée");
        }


        facture.setPayee(status); //payment
        return ResponseEntity.ok(facture);
    }

}
