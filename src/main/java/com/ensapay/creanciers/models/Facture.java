package com.ensapay.creanciers.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.time.Duration;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Facture {
    @Id
    private String id;
    private String description;
    private double montant;
    private LocalDate dateLimit;
    private boolean payee;
    private double fraisPenalite;

    private String creanceId;
    private String userId;

    public double getMontantFinal() {
        return montant + (fraisPenalite * Duration.between(LocalDate.now().atStartOfDay(), dateLimit.atStartOfDay()).toDays());
    }
}
