package com.ensapay.creanciers.repositories;


import com.ensapay.creanciers.models.Creance;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CreanceRepository extends MongoRepository<Creance,String> {
    List<Creance> findAllByCategory(String category);
}
