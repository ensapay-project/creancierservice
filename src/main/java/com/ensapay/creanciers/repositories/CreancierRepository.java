package com.ensapay.creanciers.repositories;

import com.ensapay.creanciers.models.Creance;
import com.ensapay.creanciers.models.Creancier;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CreancierRepository extends MongoRepository<Creancier,String> {

  // List<Creance> findAllByCodeCreancier(String codeCreancier);
}
