package com.ensapay.creanciers.services;

import com.ensapay.creanciers.models.Creance;
import com.ensapay.creanciers.models.Creancier;
import com.ensapay.creanciers.repositories.CreanceRepository;
import com.ensapay.creanciers.repositories.CreancierRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class CreanceService {

    private final CreanceRepository creanceRepository;
    private final CreancierRepository creancierRepository;

    public List<Creance> listAll() {
        return creanceRepository.findAll();
    }

    public void save(Creance c) {
        creanceRepository.save(c);
    }

    public Creance get(String id) {
        return creanceRepository.findById(id).orElseThrow(
                () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Creance introuvable"));
    }

    public void delete(String idCreancier , String idCreance) {
        Creance creance = get(idCreance);
        Creancier creancier = creancierRepository.findById(idCreancier).orElseThrow(
                ()->new ResponseStatusException(HttpStatus.NOT_FOUND)
        );
        creancier.getCreances().remove(creance);
        creancierRepository.save(creancier);
        creanceRepository.delete(creance);
    }
}
